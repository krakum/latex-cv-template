Author: Kranthi Lakum
Project Title: Template for Typesetting your Academic CV / Professional Resume in LaTeX
Project Category: Template
Document Type: Tex file

DISCLAIMER:
This template is provided for free and without any guarantee that it will correctly compile on your system,
if you have a non-standard configuration. This template was successfully compiled with MikTeX 2.9.

LICENSE:
Template for Typesetting your Academic CV / Professional Resume in LaTeX by Kranthi Lakum is licensed 
under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
Permissions beyond the scope of this license may be available at % http://creativecommons.org/licenses/by-nc-sa/3.0/
You are free to share, remix, but not make commercial use of the work.

INSTRUCTIONS:
Download and install MikTeX 2.9 http://miktex.org/2.9/setup
Installing Ghostscript http://pages.cs.wisc.edu/~ghost/doc/gnu/7.05/Install.htm
Installing WinEdt 7.0 http://winedt.com/installing.html
Configuring WinEdt with MikTeX http://www.haverford.edu/math/tex/wined.htm

TIPS:
Always use MikTex Package Manager to download TeX packages.
Before you download new packages, stop any MikTex instances that are running
Compile this document in XeLaTeX for correct results.
In Windows, use WinEdt 7.0 or later versions to edit the template.